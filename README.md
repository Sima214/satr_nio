# SATR Native I/O

Core module of Satr.

## Overview

Provides:

- Path utilities.
- File stats.
- Synchronous file I/O
- Mapped file.
